<?php

namespace Drupal\menu_to_taxonomy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\taxonomy\Entity\Term;

/**
 * Defines the Menu To Taxonomy entity.
 *
 * @ConfigEntityType(
 *   id = "menu_to_taxonomy",
 *   label = @Translation("Menu to taxonomy"),
 *   handlers = {
 *     "list_builder" = "Drupal\menu_to_taxonomy\Controller\MenuToTaxonomyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\menu_to_taxonomy\Form\MenuToTaxonomyForm",
 *       "edit" = "Drupal\menu_to_taxonomy\Form\MenuToTaxonomyForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "menu",
 *     "vocabulary"
 *   },
 *   config_prefix = "menu_to_taxonomy",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "edit-form" = "/admin/structure/menu_to_taxonomy/{menu_to_taxonomy}",
 *     "delete-form" = "/admin/structure/menu_to_taxonomy/{menu_to_taxonomy}/delete",
 *     "collection" = "/admin/structure/menu_to_taxonomy"
 *   }
 * )
 */
class MenuToTaxonomy extends ConfigEntityBase implements MenuToTaxonomyInterface {

  /**
   * The MenuToTaxonomy ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The MenuToTaxonomy label.
   *
   * @var string
   */
  protected $label;

  /**
   * The vocabulary to sync.
   *
   * @var string
   *   The machine name of the vocabulary this entity represents.
   */
  protected $vocabulary;

  /**
   * The source menu.
   *
   * @var string
   *   The machine name of the menu entity.
   */
  protected $menu;

  /**
   * {@inheritdoc}
   */
  public function getVocabulary() {
    return $this->vocabulary;
  }

  /**
   * {@inheritdoc}
   */
  public function getMenu() {
    return $this->menu;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $this->addDependency('config', 'taxonomy.vocabulary.' . $this->getVocabulary());
  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    // Make sure we don't have any save exceptions before building terms.
    $return = parent::save();
    $this->regenerateTerms();
    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    /** @var \Drupal\menu_to_taxonomy\MenuToTaxonomySyncRepositoryInterface $repository */
    $repository = \Drupal::service('menu_to_taxonomy.sync_repository');

    // Load taxonomy menus.
    $records = $repository->getAll($this->getVocabulary());

    /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager */
    $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

    foreach ($records as $record) {
      /** @var \Drupal\menu_link\Plugin\Menu\MenuLinkField $link */
      $link = $menu_link_manager->createInstance($record->menu_link_plugin);
      if (!empty($link)) {
        // Delete any linked terms as their lifetime is determined by the menu to taxonomy entity.
        if (getMenuName($link) == $this->menu) {
          $term = Term::load($record->tid);
          if (!empty($term)) {
            $term->delete();
          }
        }
      }
    }

    parent::delete();
  }

  /**
   * Regenerates terms for the links in the menu.
   */
  protected function regenerateTerms() {
    /** @var \Drupal\Core\Menu\MenuLinkTreeInterface $menu_link_tree */
    $menu_link_tree = \Drupal::service('menu.link_tree');

    /** @var \Drupal\menu_to_taxonomy\MenuToTaxonomyTermManagerInterface $term_manager */
    $term_manager = \Drupal::service('menu_to_taxonomy.term_manager');
    $parameters = new MenuTreeParameters();
    $menu_parts = explode(":", $this->getMenu(), 2);
    if ($menu_parts[1]) {
      $menu_name = $menu_parts[0] . ":";
      $plugin_id = $menu_parts[1];
      $parameters->setRoot($plugin_id);
      $parameters->excludeRoot();
    } else {
      $menu_name = $menu_parts[0];
    }
    $tree = $menu_link_tree->load($menu_name, $parameters);

    $tree = $menu_link_tree->transform($tree, [['callable' => 'menu.default_tree_manipulators:generateIndexAndSort']]);
    $items = $this->walkTree($tree);
    foreach ($items as $tree_item) {
      $link = $tree_item->link;
      $term_manager->saveTermAndSyncRecord(getMenuName($link), getPluginId($link));
    }
  }

  /**
   * @param \Drupal\Core\Menu\MenuLinkTreeElement[] $items
   *
   * @return \Drupal\Core\Menu\MenuLinkTreeElement[]
   */
  protected function walkTree(array $items) {
    $results = [];
    foreach ($items as $item) {
      $results[] = $item;
      if (!empty($item->subtree)) {
        $results[] = $this->walkTree($item->subtree);
      }
    }
    return $this->flatten($results);
  }

  /**
   * Flattens a multidimensional array.
   *
   * @param array $array
   *   The unflattened array.
   *
   * @return array
   *   The flattened array.
   *
   * @see http://stackoverflow.com/questions/1319903/how-to-flatten-a-multidimensional-array
   */
  protected function flatten(array $array) {
    $return = [];
    array_walk_recursive(
          $array, function ($a) use (&$return) {
              $return[] = $a;
          }
      );
    return $return;
  }

}

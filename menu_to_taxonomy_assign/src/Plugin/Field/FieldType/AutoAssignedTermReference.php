<?php

namespace Drupal\menu_to_taxonomy_assign\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'entity_reference_menu_to_taxonomy_assign' entity field type.
 *
 * Supported settings (below the definition's 'settings' key) are:
 * - target_type: The entity type to reference. Required.
 *
 * @FieldType(
 *   id = "entity_reference_menu_to_taxonomy_assign",
 *   label = @Translation("Taxonomy Term (Menu To Taxonomy Assigned)"),
 *   description = @Translation("An entity field containing a auto-assigned term reference."),
 *   category = @Translation("Reference"),
 *   default_widget = "autoassign_entity_reference_autocomplete",
 *   default_formatter = "autoassign_entity_reference_label",
 *   list_class = "\Drupal\menu_to_taxonomy_assign\AutoAssignedTermReferenceItemList",
 *   cardinality = \Drupal\Core\Field\FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED,
 * )
 */
class AutoAssignedTermReference extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'taxonomy_term',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $element['target_type']['#options'] = ['taxonomy_term' => $this->t('Taxonomy term')];
    return $element;
  }

}

<?php

namespace Drupal\menu_to_taxonomy;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Exception;

/**
 * Class MenuToTaxonomyTermManager.
 *
 * @package Drupal\menu_to_taxonomy.
 */
class MenuToTaxonomyTermManager implements MenuToTaxonomyTermManagerInterface {

  /**
   * Menu to taxonomy storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $menuToTaxonomyStorage;

  /**
   * Menu Link Manager.
   *
   * @var \Drupal\Core\Menu\MenuLinkManagerInterface
   */
  protected $manager;

  /**
   * @var MenuToTaxonomySyncRepositoryInterface
   */
  protected $syncRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Menu\MenuLinkManagerInterface $manager
   *   The menu link manager.
   *
   * @param MenuToTaxonomySyncRepositoryInterface $repository
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MenuLinkManagerInterface $manager, MenuToTaxonomySyncRepositoryInterface $repository) {
    $this->menuToTaxonomyStorage = $entity_type_manager->getStorage('menu_to_taxonomy');
    $this->manager = $manager;
    $this->syncRepository = $repository;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTermsForLink($plugin_id) {
    $vocabularies_to_update = $this->syncRepository->getSync($plugin_id);
    foreach ($vocabularies_to_update as $row) {
      $this->syncRepository->deleteForPlugin($plugin_id);
      $term = Term::load($row->tid);
      if (empty($term)) {
        return;
      }
      // No worries about the children, as these already have been reasssigned
      // to the parent of the deleted term.
      $term->delete();
    }
  }

  /**
   * Find out which vocabularies we need to update when adding a given link.
   *
   * @param string $menu_name
   *   The menu name to check vocabulary for.
   * @param string $plugin_id
   *   The menu plugin id to check vocabulary for.
   *
   * @return string[]
   *   Vocabulary IDs to update
   */
  public function getVocabulariesToUpdate($menu_name, $plugin_id) {
    $vocabularies_to_update = [];
    /** @var \Drupal\menu_to_taxonomy\Entity\MenuToTaxonomy[] $menu_to_taxonomy_collection */
    $menu_to_taxonomy_collection = $this->menuToTaxonomyStorage->loadMultiple();

    // The link parents to look for in menu_to_taxonomy configuration.
    $parents = $this->manager->getParentIds($plugin_id);
    // We add an empty item to the end of the array. This represents root menu
    // and will be used if menu_to_taxonomy matches the whole menu.
    $parents = !empty($parents) ? $parents : [];
    $parents += ['root' => ''];

    foreach ($menu_to_taxonomy_collection as $menu_to_taxonomy) {
      $menu_to_taxonomy_menu = $menu_to_taxonomy->getMenu();
      $matching_menu = array_filter($parents, function ($v) use($menu_name, $menu_to_taxonomy_menu, $plugin_id) {
        return ($menu_name.$v == $menu_to_taxonomy_menu) && ($menu_to_taxonomy_menu != $menu_name.$plugin_id);
      });

      if ($matching_menu) {
        $vocabularies_to_update[] = $menu_to_taxonomy->getVocabulary();
      }
    }

    return $vocabularies_to_update;
  }

  /**
   * {@inheritdoc}
   */
  public function delete($tid) {
    $this->syncRepository->deleteForTid($tid);
  }

  /**
   * {@inheritdoc}
   */
  public function saveTermAndSyncRecord($menu_name, $plugin_id) {
    /** @var \Drupal\Core\Menu\MenuLinkInterface $link */
    $link = $this->manager->createInstance($plugin_id);

    // Find out which vocabularies we need to update.
    $vocabularies_to_update = $this->getVocabulariesToUpdate($menu_name, $plugin_id);

    foreach ($vocabularies_to_update as $vid) {
      // Get the terms linked to this menu item for this vocabulary.
      $tid = $this->syncRepository->getTid($plugin_id, $vid);
      // Set default.
      $term = $tid ? Term::load($tid) : Term::create(['vid' => $vid]);
      if (empty($term)) {
        throw new Exception('Could not synchronize term.');
      }

      $parent_plugin_id = $link->getParent();
      if (!empty($parent_plugin_id)) {
        // Get the terms associated to the parent's menu link item ID.
        $parent_tid = $this->syncRepository->getTid($parent_plugin_id, $vid);
        if (!empty($parent_tid)) {
          $term->set('parent', $parent_tid);
        }
      }

      $term->setName($link->getTitle());
      $term->setWeight($link->getWeight());
      $term->setDescription($link->getDescription());
      $term->setPublished($link->isEnabled());

      // Save the term and its menu link item sync record.
      $this->syncRepository->doSaveTermAndSyncRecord($term, $link);
      // Clear cache for this term.
      \Drupal::entityTypeManager()->getStorage('taxonomy_term')->resetCache();
    }
  }

}

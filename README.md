# Menu to Taxonomy

This module is similar to the taxonomy_menu module (which synchronizes a
taxonomy to a menu), except it synchronizes a menu to a taxonomy instead.

Any new menu links for a specific menu will have a new, corresponding taxonomy
term if a "Menu to taxonomy" entity exists for the menu and the vocabulary.

When saving the "Menu to taxonomy" entity, the vocabulary will be automatically
populated.

This will work for both menu_link field and menu_link_content menu links.

## Submodules

The (optional) "Menu to Taxonomy Assign" submodule allows you to have Term
references that will be automatically assigned to any terms synchronized to
menu links pointing to the node containing the term reference.

See the README file inside the menu_to_taxonomy_assign submodule for further
information.

<?php

namespace Drupal\menu_to_taxonomy\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MenuToTaxonomyForm.
 *
 * @package Drupal\menu_to_taxonomy\Form
 */
class MenuToTaxonomyForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\menu_to_taxonomy\Entity\MenuToTaxonomy $menu_to_taxonomy */
    $menu_to_taxonomy = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $menu_to_taxonomy->label(),
      '#description' => $this->t("Label for the Menu To Taxonomy."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $menu_to_taxonomy->id(),
      '#machine_name' => [
        'exists' => '\Drupal\menu_to_taxonomy\Entity\MenuToTaxonomy::load',
      ],
      '#disabled' => !$menu_to_taxonomy->isNew(),
    ];

    // Menu selection.
    /** @var \Drupal\Core\Menu\MenuParentFormSelector $menu_parent_selector */
    $menu_parent_selector = \Drupal::service('menu.parent_form_selector');
    $form['menu'] = $menu_parent_selector->parentSelectElement($menu_to_taxonomy->getMenu());
    $form['menu'] += [
      '#title' => $this->t('Menu to sync from'),
    ];

    // Vocabulary selection.
    $options = [];
    $vocabulary_storage = \Drupal::entityTypeManager()->getStorage('taxonomy_vocabulary');
    foreach ($vocabulary_storage->loadMultiple() as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->label();
    }
    $form['vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary to sync to'),
      '#options' => $options,
      '#default_value' => $menu_to_taxonomy->getVocabulary(),
      '#ajax' => [
        'callback' => '::ajaxReplaceDescriptionFieldForm',
        'wrapper' => 'description-field-container',
        'method' => 'replace',
      ],
    ];

    $form['help_text'] = [
      '#markup' => $this->t('Clicking "Save" will repopulate the "Vocabulary to sync to" with all the menu links in the selected "Menu to sync from".'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $menu_to_taxonomy = $this->entity;
    $status = $menu_to_taxonomy->save();

    if ($status) {
      $this->messenger()->addStatus($this->t(
          'Saved the %label Menu To Taxonomy.', [
            '%label' => $menu_to_taxonomy->label(),
          ]
      ));
    }
    else {
      $this->messenger()->addStatus($this->t(
          'The %label Menu To Taxonomy was not saved.', [
            '%label' => $menu_to_taxonomy->label(),
          ]
      ));
    }
    $form_state->setRedirectUrl($menu_to_taxonomy->toUrl('collection'));
  }

}

<?php

namespace Drupal\menu_to_taxonomy_assign;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Defines a item list class for entity reference fields.
 */
class AutoAssignedTermReferenceItemList extends EntityReferenceFieldItemList implements AutoAssignedTermReferenceItemListInterface {

  /**
   * {@inheritdoc}
   */
  public function updateAutoAssigned() {
    $config = \Drupal::config('menu_to_taxonomy_assign.settings');
    if ($config->get('sync_menu_terms')) {
      $values = [];
      $hasNewValues = TRUE;
    } else {
      $values = parent::getValue();
      $hasNewValues = FALSE;
    }

    $entity = $this->getEntity();
    if ($entity->getEntityTypeId() == 'node') {
      $link = $this->getLinkFromNode($entity);
      if (empty($link)) {
        return;
      }
      /** @var \Drupal\menu_to_taxonomy\MenuToTaxonomySyncRepositoryInterface $service */
      $service = \Drupal::service('menu_to_taxonomy.sync_repository');

      $id = $link->getMenuPluginId();
      $menu_name = $link->get('menu_name')->getValue();
      /** @var \Drupal\menu_to_taxonomy\MenuToTaxonomyTermManager $term_manager */
      $term_manager = \Drupal::service('menu_to_taxonomy.term_manager');
      // For the current selected menu, let's get the corresponding taxonomy
      // based on the menu_to_taxonomy settings.
      $vocabulary_to_update = $term_manager->getVocabulariesToUpdate($menu_name . ':', $id);
      $vids = '';
      if (!empty($vocabulary_to_update)) {
        $vids = $vocabulary_to_update;
      } else {
        // Fallback by checking all the target bundles.
        $settings = $this->getSettings();
        if (isset($settings['handler_settings']['target_bundles'])) {
          $vids = array_values($settings['handler_settings']['target_bundles']);
        }
      }
      if (!empty($vids)) {
        foreach ($vids as $vid) {
          $tid = $service->getTid($id, $vid);
          if (empty($tid) || in_array(
              $tid, array_map(
                function ($x) {
                  return $x['target_id'];
                }, $values
              )
            )
          ) {
            continue;
          }
          $term = Term::load($tid);
          if (empty($term)) {
            continue;
          }
          if (!$hasNewValues) {
            $hasNewValues = TRUE;
          }
          $values[] = $tid;
        }
      }
    }
    if ($hasNewValues) {
      $this->setValue($values, FALSE);
    }
    return $hasNewValues;
  }

  protected function getLinkFromNode(NodeInterface $node) {
    if (!$node->hasField('field_menulink')) {
      return FALSE;
    }
    /** @var \Drupal\menu_link\Plugin\Field\FieldType\MenuLinkItem $link */
    $link = $node->get('field_menulink')[0];
    if (empty($link)) {
      return FALSE;
    }
    return $link;
  }

}

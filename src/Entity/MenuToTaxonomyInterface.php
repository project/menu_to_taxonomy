<?php

namespace Drupal\menu_to_taxonomy\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a MenuToTaxonomy entity.
 */
interface MenuToTaxonomyInterface extends ConfigEntityInterface {

  /**
   * Get the menu that the menu items are being created in.
   *
   * @return string
   *   The machine name of the menu entity holding the vocabulary's menu items.
   */
  public function getMenu();

  /**
   * Get the vocabulary being used.
   *
   * @return string
   *   The vocabulary whose terms will be used to generate a menu.
   */
  public function getVocabulary();

}

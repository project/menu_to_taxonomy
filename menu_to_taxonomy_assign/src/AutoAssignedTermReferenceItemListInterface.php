<?php

namespace Drupal\menu_to_taxonomy_assign;

interface AutoAssignedTermReferenceItemListInterface {

  /**
   * Updates the list of terms to include the term synchronized to the current
   * menu link for the node.
   *
   * @return bool
   *   Whether the field has new values.
   */
  public function updateAutoAssigned();

}

<?php


namespace Drupal\menu_to_taxonomy;

use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\taxonomy\TermInterface;

/**
 *
 */
interface MenuToTaxonomySyncRepositoryInterface {

  /**
   * Upserts a Menu to Taxonomy sync record.
   *
   * @param array $record
   *   The Menu to taxonomy sync record.
   *
   * @throws \Exception
   */
  public function upsert(array $record);

  /**
   * Deletes the sync record for the given tid.
   *
   * @param int $tid
   */
  public function deleteForTid($tid);

  /**
   * Deletes the sync record for the given menu plugin id.
   *
   * @param string $menu_link_plugin
   */
  public function deleteForPlugin($menu_link_plugin);

  /**
   * Gets the term for a given menu link and vocabulary.
   *
   * @param string $menu_link_plugin
   *   The menu link plugin ID.
   * @param string $vid
   *   The vocabulary ID.
   *
   * @return int
   *   The term ID.
   */
  public function getTid($menu_link_plugin, $vid);

  /**
   * Gets the vocabulary for a given menu link.
   *
   * @param string $menu_link_plugin
   *   The menu link plugin ID.
   *
   * @return array
   *   The vocabulary and term ID.
   */
  public function getSync($menu_link_plugin);

  /**
   * @param string $vid
   *   Vocabulary ID.
   *
   * @return all records
   */
  public function getAll($vid);

  /**
   * Saves a term and its menu link item sync record.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   */
  public function doSaveTermAndSyncRecord(TermInterface $term, MenuLinkInterface $link);

}

<?php

namespace Drupal\menu_to_taxonomy;

/**
 * Class MenuToTaxonomyTermManager.
 *
 * @package Drupal\menu_to_taxonomy
 */
interface MenuToTaxonomyTermManagerInterface {

  /**
   * Clean up when a link is deleted.
   *
   * @param string $pluginId
   *   The menu link plugin id.
   */
  public function deleteTermsForLink($pluginId);

  /**
   * Clean up when a term is deleted.
   *
   * @param int $tid
   */
  public function delete($tid);

  /**
   * Updates/creates a linked term and saves a DB record for the menu link
   * plugin/tid link.
   *
   * @param string $menu_name
   *   The menu name.
   * @param string $pluginId
   *   The menu link plugin id.
   */
  public function saveTermAndSyncRecord($menu_name, $pluginId);

}

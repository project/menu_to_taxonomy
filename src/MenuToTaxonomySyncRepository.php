<?php

namespace Drupal\menu_to_taxonomy;

use Drupal\Core\Database\Connection;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\taxonomy\TermInterface;

/**
 *
 */
class MenuToTaxonomySyncRepository implements MenuToTaxonomySyncRepositoryInterface {
  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a new MenuToTaxonomySyncManager.
   *
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public function upsert(array $record) {
    if (empty($record['menu_link_plugin']) || empty($record['tid']) || empty($record['vid'])) {
      throw new Exception(t('Incorrect Menu to taxonomy sync record.'));
    }
    $this->connection->upsert('menu_to_taxonomy_sync')
      ->key('tid')
      ->fields(array_keys($record))
      ->values($record)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteForTid($tid) {
    $this->connection->delete('menu_to_taxonomy_sync')
      ->condition('tid', $tid)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteForPlugin($menu_link_plugin) {
    $this->connection->delete('menu_to_taxonomy_sync')
      ->condition('menu_link_plugin', $menu_link_plugin)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getTid($menu_link_plugin, $vid) {
    return $this->connection->select('menu_to_taxonomy_sync', 'm')
      ->fields('m', ['tid'])
      ->condition('menu_link_plugin', $menu_link_plugin)
      ->condition('vid', $vid)
      ->execute()
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function getSync($menu_link_plugin) {
    return $this->connection->select('menu_to_taxonomy_sync', 'm')
      ->fields('m', ['vid', 'tid'])
      ->condition('menu_link_plugin', $menu_link_plugin)
      ->execute()
      ->fetchAll();
  }

  public function getAll($vid) {
    return $this->connection->select('menu_to_taxonomy_sync')
      ->fields('menu_to_taxonomy_sync', ['tid', 'menu_link_plugin'])
      ->condition('vid', $vid)
      ->execute()
      ->fetchAll();
  }
  /**
   * {@inheritdoc}
   */
  public function doSaveTermAndSyncRecord(TermInterface $term, MenuLinkInterface $link) {
    $transaction = $this->connection->startTransaction();
    try {
      // Save the term.
      $term->save();
      // Save the link between the menu link item <=> term ID.
      $this->upsert(
            [
              'tid' => $term->id(),
              'vid' => $term->bundle(),
              'menu_link_plugin' => getPluginId($link),
            ]
        );
    }
    catch (Exception $e) {
      $transaction->rollback();
      throw $e;
    }
  }

}

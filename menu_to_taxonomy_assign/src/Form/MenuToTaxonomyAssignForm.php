<?php

namespace Drupal\menu_to_taxonomy_assign\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class MenuToTaxonomyAssignForm.
 *
 * @package Drupal\menu_to_taxonomy_assign\Form
 */
class MenuToTaxonomyAssignForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'menu_to_taxonomy_assign.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'menu_to_taxonomy_assign_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('menu_to_taxonomy_assign.settings');

    $form['sync_menu_terms'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sync menu and terms'),
      '#description' => $this->t('Keep the node taxonomy field syncronized with the menu items.'),
      '#default_value' => $config->get('sync_menu_terms'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('menu_to_taxonomy_assign.settings')
      ->set('sync_menu_terms', $form_state->getValue('sync_menu_terms'))
      ->save();
  }
}


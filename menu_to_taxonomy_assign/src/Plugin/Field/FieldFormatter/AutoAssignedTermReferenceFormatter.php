<?php

namespace Drupal\menu_to_taxonomy_assign\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceLabelFormatter;

/**
 * Plugin implementation of the 'entity reference label' formatter.
 *
 * @FieldFormatter(
 *   id = "autoassign_entity_reference_label",
 *   label = @Translation("Label"),
 *   description = @Translation("Display the label of the referenced entities."),
 *   field_types = {
 *     "entity_reference_menu_to_taxonomy_assign"
 *   }
 * )
 */
class AutoAssignedTermReferenceFormatter extends EntityReferenceLabelFormatter {

}

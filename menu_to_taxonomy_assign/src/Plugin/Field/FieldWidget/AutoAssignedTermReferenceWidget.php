<?php

namespace Drupal\menu_to_taxonomy_assign\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Plugin implementation of the 'entity_reference_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "autoassign_entity_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "entity_reference_menu_to_taxonomy_assign"
 *   },
 * )
 */
class AutoAssignedTermReferenceWidget extends EntityReferenceAutocompleteWidget {

}

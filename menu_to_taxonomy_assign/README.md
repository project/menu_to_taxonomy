# Menu to Taxonomy Assign

To use, enable the module, then add a entity reference field of type "Taxonomy term (Menu to Taxonomy assigned)" on
any node types you would like to have auto-assigned term references on.

In the field settings, select any vocabularies you would like to auto-assign Menu To Taxonomy-synchronized terms from.

Upon saving any node with a Menu to Taxonomy assigned taxonomy term reference field, the term reference will be updated
to include the term synced with the menu link in your menu link field.

## Requirements
- [Menu link](https://www.drupal.org/project/menu_link) module
